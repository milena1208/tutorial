import java.util.*;
/*
public class Main {
    public static void main(String[] args) {
        int[] array = {4, 6, 3, 6, 8, 9, 12};
        Main(array);
    }

    public static void Main(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(Math.log(1000 * 100000));
        }
    }
}
*/
/*
import java.util.*;
public class Main {
    public static void main(String[] args) {
        int[] array = {4, 6, 3, 6, 8, 9, 12};
        Main mainObject = new Main(); // Tworzymy obiekt klasy Main
        mainObject.printLogs(array); // Wywołujemy metodę printLogs na tym obiekcie
        int newValue = 200000 * 4000000 / 2; // Deklarujemy i przypisujemy wartość zmiennej newValue#
        // Wyświetlamy wartość zmiennej newValue
        System.out.println("newValue: " + newValue);
    }

    public void printLogs(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(Math.log(1000 * 100000));
        }
    }
}
*/
/*
// logarithmic time algorithm
public class Main {
    public static void main(String[] args) {
        int n = 8;
        for (int i = 1; i < n; i = i * 2) {
            System.out.println("Hey - I'm busy looking at: " + i);
        }
    }
}
*/
/*
//Polynomial Time algorithm O(n^p)b(printed 64 times, 1 and 1, 1 and 2 itd...8 and 8
public class Main {
    public static void main(String[] args) {
        int n = 8;
    for(int i=1;i<=n;i++) {
        for(int j=1; j<=n; j++){
            System.out.println("Hey, busy at: " + i + "and" + j);
            }
        }
    }
}
*/
/*
//Polynomial Time algorithm O(n^p) - (printed 16 times, 0 and 0, 0 and 1. .. 0 and 3, 1 and 0...3 and 3
//The outer loop executes n times, , the inner loop executes M times.inner loop execute a total of N * M times. O(N^2).
public class Main {
    public static void main(String[] args) {
        int n = 4;
        for(int i=0;i<n;i++) {
            for(int j=0; j<n; j++){
                System.out.println("Hey, busy at: " + i + "and" + j);
            }
        }
    }
}
*/
// Bubble Sort: 3x + 9y + 8z = 79; x, y, and z < n; cubic running time: O(n^3)
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<int[]> solutions = findXYZ(10);
        for (int[] solution : solutions) {
            System.out.println("x: " + solution[0] + ", y: " + solution[1] + ", z: " + solution[2]);
        }
    }

    public static ArrayList<int[]> findXYZ(int n) {
        ArrayList<int[]> solutions = new ArrayList<>();
        for (int x = 0; x < n; x++) {
            for (int y = 0; y < n; y++) {
                for (int z = 0; z < n; z++) {
                    if (3 * x + 9 * y + 8 * z == 79) {
                        int[] solution = {x, y, z};
                        solutions.add(solution);
                    }
                }
            }
        }
        return solutions;
    }
}

